let flag = true;

const func1 = () => {
    if(flag == true) {
        document.getElementById('inp1').value = "X";
        document.getElementById('inp1').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp1').value = "0";
        document.getElementById('inp1').disabled = true;
        flag = true;
    }
}

const func2 = () => {
    if(flag == true) {
        document.getElementById('inp2').value = "X";
        document.getElementById('inp2').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp2').value = "0";
        document.getElementById('inp2').disabled = true;
        flag = true;
    }
}

const func3 = () => {
    if(flag == true) {
        document.getElementById('inp3').value = "X";
        document.getElementById('inp3').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp3').value = "0";
        document.getElementById('inp3').disabled = true;
        flag = true;
    }
}

const func4 = () => {
    if(flag == true) {
        document.getElementById('inp4').value = "X";
        document.getElementById('inp4').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp4').value = "0";
        document.getElementById('inp4').disabled = true;
        flag = true;
    }
}

const func5 = () => {
    if(flag == true) {
        document.getElementById('inp5').value = "X";
        document.getElementById('inp5').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp5').value = "0";
        document.getElementById('inp5').disabled = true;
        flag = true;
    }
}

const func6 = () => {
    if(flag == true) {
        document.getElementById('inp6').value = "X";
        document.getElementById('inp6').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp6').value = "0";
        document.getElementById('inp6').disabled = true;
        flag = true;
    }
}

const func7 = () => {
    if(flag == true) {
        document.getElementById('inp7').value = "X";
        document.getElementById('inp7').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp7').value = "0";
        document.getElementById('inp7').disabled = true;
        flag = true;
    }
}

const func8 = () => {
    if(flag == true) {
        document.getElementById('inp8').value = "X";
        document.getElementById('inp8').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp8').value = "0";
        document.getElementById('inp8').disabled = true;
        flag = true;
    }
}

const func9 = () => {
    if(flag == true) {
        document.getElementById('inp9').value = "X";
        document.getElementById('inp9').disabled = true;
        flag = false;
    } else {
        document.getElementById('inp9').value = "0";
        document.getElementById('inp9').disabled = true;
        flag = true;
    }
}

const func = () => {
    let inp1 = document.getElementById('inp1').value;
    let inp2 = document.getElementById('inp2').value;
    let inp3 = document.getElementById('inp3').value;
    let inp4 = document.getElementById('inp4').value;
    let inp5 = document.getElementById('inp5').value;
    let inp6 = document.getElementById('inp6').value;
    let inp7 = document.getElementById('inp7').value;
    let inp8 = document.getElementById('inp8').value;
    let inp9 = document.getElementById('inp9').value;

    if((inp1 == 'X' && inp5 == 'X' && inp9 == 'X') ||
    (inp1 == 'X' && inp4 == 'X' && inp7 == 'X') ||
    (inp1 == 'X' && inp2 == 'X' && inp3 == 'X') ||
    (inp4 == 'X' && inp5 == 'X' && inp6 == 'X') ||
    (inp7 == 'X' && inp8 == 'X' && inp9 == 'X') ||
    (inp3 == 'X' && inp5 == 'X' && inp7 == 'X') ||
    (inp2 == 'X' && inp5 == 'X' && inp8 == 'X') ||
    (inp3 == 'X' && inp6 == 'X' && inp9 == 'X')) {
        let text = "Player X won";
        promise(text);
    }

    if((inp1 == '0' && inp5 == '0' && inp9 == '0') ||
    (inp1 == '0' && inp4 == '0' && inp7 == '0') ||
    (inp1 == '0' && inp2 == '0' && inp3 == '0') ||
    (inp4 == '0' && inp5 == '0' && inp6 == '0') ||
    (inp7 == '0' && inp8 == '0' && inp9 == '0') ||
    (inp3 == '0' && inp5 == '0' && inp7 == '0') ||
    (inp2 == '0' && inp5 == '0' && inp8 == '0') ||
    (inp3 == '0' && inp6 == '0' && inp9 == '0')) {
        let text = "Player 0 won";
        promise(text);
    }
}

const reset = () => {
    location.reload();
}

const promise = (text) => {
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            window.alert(text);
            resolve();
        }, 500);
    })

    promise.then(() => {
        console.log("Hello World");
        reset();
    })
}


